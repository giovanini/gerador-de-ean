var os = require('os');
var databaseDir = os.homedir();
if(os.platform() == 'win32'){
  databaseDir = process.env.APPDATA;
}
var low = require('lowdb');
var db = low(databaseDir + '/ean.json');
var config = require('./config/config').build(db);

document.addEventListener('DOMContentLoaded', function () {

  var loadConfig = function(cfg){
    var configurationItems = document.querySelectorAll('.configuration-item');
    for (var i = 0; i < configurationItems.length; ++i) {
      var configurationItem = configurationItems[i];
      if(configurationItem.type == 'checkbox'){
        configurationItem.checked = config.read(configurationItem.id);
      }else{
        configurationItem.value = config.read(configurationItem.id);
      }
    }
  }(config);

  document.querySelector('#generate-ean-button').addEventListener('click', function(){
    var ean_generator = require('./ean/generate');
    var copy = require('copy-to-clipboard');

    var eanContainer = document.querySelector('#ean-code-container');

    var countryCode = config.read('country-code-text');
    var useRamdomcountryCode = config.read('use-ramdom-country-code-swicth');
    if(useRamdomcountryCode){
      countryCode = Math.floor(Math.random() * (999))
    }

    var manufacturerId = config.read('manufacturer-id-text');
    var useRamdomManufacturerId = config.read('use-ramdom-manufacturer-id-swicth');
    if(useRamdomManufacturerId){
      manufacturerId = Math.floor(Math.random() * (99999))
    }

    var productId = Math.floor(Math.random() * (9999));

    var ean = ean_generator.generate(countryCode,manufacturerId,productId);
    var copyOnGenerate = config.read('copy-on-generate-swicth');

    var savedEan = db.get('ean').find({ 'number': ean }).value();
    if(savedEan){
      eanContainer.innerHTML = "EAN já existe. Gerar outro.";
    }else{
      if(copyOnGenerate){
        copy(ean);
      }
      db.get('ean').push({ 'number': ean}).value();
      eanContainer.innerHTML = ean;
    }
  });

  var configurationItems = document.querySelectorAll('.configuration-item');
  for (var i = 0; i < configurationItems.length; ++i) {
    var configurationItem = configurationItems[i];
    configurationItem.addEventListener('change', function() {
      if(this.type == 'checkbox'){
        config.write(this.id, this.checked);
      }else{
        config.write(this.id, this.value);
      }
    });
  }
});
