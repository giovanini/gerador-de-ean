var app = require('app');  // Module to control application life.
var BrowserWindow = require('browser-window');  // Module to create native browser window.
var ipc = require('ipc');
var fs = require('fs');
var low = require('lowdb');
var config = require('./config/config');
var os = require('os');

var databaseDir = os.homedir();
if(os.platform() == 'win32'){
  databaseDir = process.env.APPDATA;
}

var db = low(databaseDir + '/ean.json');
db.defaults({ ean: [], config: []}).value();
config.init(db, {
  'country-code-text': '789',
  'manufacturer-id-text': '00001',
  'use-ramdom-country-code-swicth': false,
  'use-ramdom-manufacturer-id-swicth': false,
  'copy-on-generate-swicth': false
});

// Report crashes to our server.
//require('crash-reporter').start();

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var mainWindow = null;

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform != 'darwin') {
    app.quit();
  }
});

// read the file and send data to the render process
//ipc.on('get-file-data', function(event) {
//  var data = null;
//  if (process.platform == 'win32' && process.argv.length >= 2) {
//    var openFilePath = process.argv[1];
//    data = fs.readFileSync(openFilePath, 'utf-8');
//  }
//  event.returnValue = data;
//});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', function() {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 329, height: 530});

  // and load the index.html of the app.
  mainWindow.loadUrl('file://' + __dirname + '/index.html');

  // Open the DevTools.
  //mainWindow.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
});
