'use strict';

var build = function (db) {
    return new Config(db);
};

var init = function(db,defaults){
  for(var property in defaults){
    if(defaults.hasOwnProperty(property)){
      var propertyOnDb = db.get('config').find({ 'property': property }).value();
      if(propertyOnDb == undefined){
        var value = defaults[property];
        db.get('config').push({ 'property': property, 'value': value, 'default': value }).value();
      }
    }
  }
}

class Config {
  constructor(db){
    this.db = db;
    this.config = {};
  }

  read(property) {
    var property = this.db.get('config').find({ 'property': property }).value();
    return property.value;
  }

  write(property, value) {
    if(value == ''){
      var cfg = this.db.get('config').find({ 'property': property }).value();
      value = cfg.default;
    }
    return this.db.get('config').find({
        'property': property
      }).assign({
          'property': property,
          'value': value
      }).value();
  }
}

module.exports.build = build;
module.exports.init = init;
