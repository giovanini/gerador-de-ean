'use strict';

var generate = function (countryCode,manufacturerId,productId) {
    return new Ean13(countryCode,manufacturerId,productId);
};

class Ean13{
  constructor(countryCode, manufacturerId, productId){
    var countryCode = String("000" + countryCode).slice(-3);
    var manufacturerId = String("00000" + manufacturerId).slice(-5);
    var productId = String("0000" + productId).slice(-4);
    var validationCode = this.generateCheckDigit(countryCode + manufacturerId + productId);
    this.value = countryCode + manufacturerId + productId + validationCode;
  }

  generateCheckDigit(eanWithoutCheckDigit){
    var evenSum = 0,
        oddSum = 0,
        sum = 0,
        nextMultipleOf10 = 0;

    for(var c = 0; c < 12;  c++){
      if((c % 2) == 0){
        evenSum += parseInt(eanWithoutCheckDigit[c]);
      }else{
        oddSum += parseInt(eanWithoutCheckDigit[c]);
      }
    }

    sum = evenSum + (oddSum * 3);
    if((sum % 10) == 0){
      return 0;
    }

    nextMultipleOf10 = (Math.floor((sum/10))+1)*10;
    return String(nextMultipleOf10 - sum);
  }

  toString(){
    return this.value;
  }
}

module.exports.generate = generate;
